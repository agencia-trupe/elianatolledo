<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome' => 'Eliana Tolledo',
            'email' => 'contato@trupe.net',
            'telefone' => '11 2613∙6484',
            'endereco' => 'Rua Dr. Sodré 122 ∙ cj 44',
            'bairro' => 'Vila Nova Conceição',
            'cep' => '04535-110',
            'cidade' => 'São Paulo, SP',
            'instagram' => 'https://www.instagram.com/elianatoledoarquitetura/',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.3518144526042!2d-46.676037985716114!3d-23.59171238466777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57571485a1b7%3A0xa3bd6f2fa0308885!2sR.%20Dr.%20Sodr%C3%A9%2C%20122%20-%20Vila%20Nova%20Concei%C3%A7%C3%A3o%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004535-110!5e0!3m2!1spt-BR!2sbr!4v1598647517622!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
            'imagem' => '',

        ]);
    }
}
