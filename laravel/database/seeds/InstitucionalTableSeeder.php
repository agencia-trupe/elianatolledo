<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstitucionalTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'imagem' => '',
            'texto_pt_um' => '',
            'texto_destaque' => '',
            'texto_pt_dois' => '',
        ]);
    }
}
