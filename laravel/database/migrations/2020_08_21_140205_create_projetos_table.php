<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos');
    }
}
