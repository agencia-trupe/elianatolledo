<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        // $router->model('clipping', 'App\Models\Clipping');
        // $router->model('imagens_clipping', 'App\Models\ClippingImagem');
        $router->model('projetos', 'App\Models\Projeto');
        $router->model('imagens_projetos', 'App\Models\ProjetoImagem');
        $router->model('institucional', 'App\Models\Institucional');
        $router->model('equipe', 'App\Models\Equipe');
        $router->model('trabalhos', 'App\Models\Trabalho');
        $router->model('banners', 'App\Models\Banner');
        $router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('projeto_slug', function ($slug) {
            return \App\Models\Projeto::whereSlug($slug)->firstOrFail();
        });

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
