<?php

namespace App\Http\Controllers;

use App\Models\Projeto;
use App\Models\ProjetoImagem;

class ProjetosController extends Controller
{
    public function index()
    {
        $projetos = Projeto::with('imagens')->ordenados()->get();

        return view('frontend.projetos', compact('projetos'));
    }

    public function show($id)
    {
        $projeto = Projeto::find($id);
        $imagens = ProjetoImagem::where('projeto_id', $id)->ordenados()->get();

        return view('frontend.projeto-show', compact('projeto', 'imagens'));
    }
}
