<?php

namespace App\Http\Controllers;

use App\Models\Banner;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        
        return view('frontend.home', compact('banners'));
    }
}
