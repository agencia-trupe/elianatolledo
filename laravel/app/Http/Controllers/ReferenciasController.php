<?php

namespace App\Http\Controllers;

use App\Models\Referencia;

class ReferenciasController extends Controller
{
    public function index()
    {
        $referencias = Referencia::ordenados()->get();

        return view('frontend.referencias', compact('referencias'));
    }
}
