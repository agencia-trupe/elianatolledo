<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReferenciasRequest;
use App\Models\Referencia;

class ReferenciasController extends Controller
{
    public function index()
    {
        $registros = Referencia::ordenados()->get();

        return view('painel.referencias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.referencias.create');
    }

    public function store(ReferenciasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Referencia::upload_imagem();

            Referencia::create($input);

            return redirect()->route('painel.referencias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($registro)
    {
        $registro = Referencia::find($registro);

        return view('painel.referencias.edit', compact('registro'));
    }

    public function update(ReferenciasRequest $request, $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Referencia::upload_imagem();

            $findRegistro = Referencia::find($registro);
            $findRegistro->update($input);

            return redirect()->route('painel.referencias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($registro)
    {
        try {
            
            $findRegistro = Referencia::find($registro);
            $findRegistro->delete();

            return redirect()->route('painel.referencias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
