<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrabalhosRequest;
use App\Models\Trabalho;

class TrabalhosController extends Controller
{
    public function index()
    {
        $registros = Trabalho::ordenados()->get();

        return view('painel.trabalhos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.trabalhos.create');
    }

    public function store(TrabalhosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Trabalho::upload_imagem();

            Trabalho::create($input);

            return redirect()->route('painel.trabalhos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Trabalho $registro)
    {
        return view('painel.trabalhos.edit', compact('registro'));
    }

    public function update(TrabalhosRequest $request, Trabalho $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Trabalho::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.trabalhos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Trabalho $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.trabalhos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
