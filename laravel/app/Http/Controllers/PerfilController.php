<?php

namespace App\Http\Controllers;

use App\Models\Institucional;
use App\Models\Equipe;

class PerfilController extends Controller
{
    public function index()
    {
        $institucional = Institucional::first();
        $equipe = Equipe::ordenados()->get();

        return view('frontend.quem-somos', compact('institucional', 'equipe'));
    }
}
