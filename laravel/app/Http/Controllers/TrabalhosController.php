<?php

namespace App\Http\Controllers;

use App\Models\Trabalho;

class TrabalhosController extends Controller
{
    public function index()
    {
        $registros = Trabalho::ordenados()->get();

        return view('frontend.como-trabalhamos', compact('registros'));
    }
}
