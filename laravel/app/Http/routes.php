<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('quem-somos', 'PerfilController@index')->name('quem-somos');
    Route::get('como-trabalhamos', 'TrabalhosController@index')->name('como-trabalhamos');
    Route::get('projetos', 'ProjetosController@index')->name('projetos.index');
    Route::get('projetos/{id}', 'ProjetosController@show')->name('projetos.show');
    Route::get('referencias-clientes', 'ReferenciasController@index')->name('referencias-clientes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('banners', 'BannersController');
        Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
        Route::resource('equipe', 'EquipeController');
        Route::resource('trabalhos', 'TrabalhosController');
        Route::resource('referencias', 'ReferenciasController');
        Route::resource('projetos', 'ProjetosController', ['except' => 'show']);
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController', ['parameters' => ['imagens' => 'imagens_projetos']]);
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');
        
//         Route::get('generator', 'GeneratorController@index')->name('generator.index');
//         Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});