<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReferenciasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'texto'  => 'required',
            'nome'   => 'required',
            'titulo' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
