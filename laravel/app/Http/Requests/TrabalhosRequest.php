<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TrabalhosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'titulo' => 'required',
            'texto'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
