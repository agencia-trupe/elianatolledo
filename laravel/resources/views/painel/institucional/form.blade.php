@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_pt_um', 'Texto (ao lado da imagem)') !!}
    {!! Form::textarea('texto_pt_um', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_destaque', 'Texto (destaque)') !!}
    {!! Form::textarea('texto_destaque', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_pt_dois', 'Texto (abaixo do texto destaque)') !!}
    {!! Form::textarea('texto_pt_dois', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}