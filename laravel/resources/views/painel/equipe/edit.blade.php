@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Equipe /</small> Editar Pessoa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.equipe.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.equipe.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
