@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.banners.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.banners.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
