<ul class="nav navbar-nav">
  <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
    <a href="{{ route('painel.banners.index') }}">Home</a>
  </li>
  <li @if(Tools::routeIs('painel.institucional*')) class="active" @endif>
    <a href="{{ route('painel.institucional.index') }}">Quem Somos</a>
  </li>
  <li @if(Tools::routeIs('painel.equipe*')) class="active" @endif>
    <a href="{{ route('painel.equipe.index') }}">Equipe</a>
  </li>
  <li @if(Tools::routeIs('painel.trabalhos*')) class="active" @endif>
    <a href="{{ route('painel.trabalhos.index') }}">Como Trabalhamos</a>
  </li>
  <li @if(Tools::routeIs('painel.projetos*')) class="active" @endif>
    <a href="{{ route('painel.projetos.index') }}">Projetos Desenvolvidos</a>
  </li>
  <li @if(Tools::routeIs('painel.referencias*')) class="active" @endif>
    <a href="{{ route('painel.referencias.index') }}">Referências e Clientes</a>
  </li>
  <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      Contato
      @if($contatosNaoLidos >= 1)
      <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
      @endif
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
      </li>
      <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
        <a href="{{ route('painel.contato.recebidos.index') }}">
          Contatos Recebidos
          @if($contatosNaoLidos >= 1)
          <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
          @endif
        </a>
      </li>
    </ul>
  </li>

</ul>