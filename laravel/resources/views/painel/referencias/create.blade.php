@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Referências e Clientes /</small> Adicionar Referência</h2>
    </legend>

    {!! Form::open(['route' => 'painel.referencias.store', 'files' => true]) !!}

        @include('painel.referencias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
