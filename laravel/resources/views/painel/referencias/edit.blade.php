@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Referências e Clientes /</small> Editar Referência</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.referencias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.referencias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
