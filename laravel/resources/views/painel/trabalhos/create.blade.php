@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Como Trabalhamos /</small> Adicionar Trabalho</h2>
    </legend>

    {!! Form::open(['route' => 'painel.trabalhos.store', 'files' => true]) !!}

        @include('painel.trabalhos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
