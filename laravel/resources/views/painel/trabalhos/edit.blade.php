@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Como Trabalhamos /</small> Editar Trabalho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.trabalhos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.trabalhos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
