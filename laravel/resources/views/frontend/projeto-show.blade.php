@extends('frontend.common.template')

@section('content')

<div class="projetos-show">
    <a href="{{ route('projetos.index') }}" class="btn-fechar">X</a> 
    <div class="center">
        <p class="projeto-titulo">{!! $projeto->titulo !!}</p>
        @foreach($imagens as $imagem)
        <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="projeto-imagens" alt="">
        @endforeach
        <a href="{{ route('projetos.index') }}" class="btn-voltar">
            « fechar e voltar </a> 
    </div> 
</div> 

@endsection