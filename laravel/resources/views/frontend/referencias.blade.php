@extends('frontend.common.template')

@section('content')

<div class="referencias">
    <div class="center masonry-grid">
        @foreach($referencias as $referencia)
        <div class="grid-item referencia">
            <img src="{{ asset('assets/img/referencias/'.$referencia->imagem) }}" class="ref-imagem" alt="">
            <div class="ref-texto">
                {!! $referencia->texto !!}
            </div>
            <div class="ref-nome-titulo">
                <p class="nome">{!! $referencia->nome !!}</p>
                <p class="titulo">{!! $referencia->titulo !!}</p>
            </div>
        </div>
        @endforeach
        <button class="btn-referencias">Ver mais +</button>
    </div>
</div>

@endsection