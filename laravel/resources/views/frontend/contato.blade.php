@extends('frontend.common.template')

@section('content')

<div class="content contato">
    <div class="center">
        <div class="contato-img">
            <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
        </div>

        <div class="contato-info">
            <p class="telefone">{{ $contato->telefone }}</p>
            <div class="end-completo">
                <p>{{ $contato->endereco }}</p>
                <p>{{ $contato->bairro }}</p>
                <p>{{ $contato->cidade }}</p>
                <p>{{ $contato->cep }}</p>
            </div>
            <a href="{{ $contato->instagram }}" target="_blank" class="instagram">
                <img src="{{ asset('assets/img/layout/marca-instagram.svg') }}" alt="">
                <p>@elianatolledoarquitetura</p>
            </a>
        </div>

        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}
            <div class="dados">
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
            </div>

            <button type="submit" class="btn-contato">ENVIAR</button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>Mensagem enviada com sucesso!</p>
                <a href="{{ route('contato') }}">« voltar</a>
            </div>
            @endif
        </form>
    </div>
</div>

@endsection

@section('fullWidthContent')

<div class="contato-mapa">
    {!! $contato->google_maps !!}
</div>

@endsection