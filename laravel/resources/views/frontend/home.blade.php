@extends('frontend.common.template')

@section('fullWidthContent')

<div class=cycle-pager></div>
<div class="banners">
    @foreach($banners as $banner)
        @if(!empty($banner->link))
        <a href="{{ $banner->link }}" class="banner link" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})"></a>
        @else
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
        @endif
    @endforeach
</div>

@endsection

