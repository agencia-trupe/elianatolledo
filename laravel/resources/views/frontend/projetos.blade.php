@extends('frontend.common.template')

@section('content')

<div class="projetos">
    <div class="center">
        @foreach($projetos as $projeto)
        <a href="{{ route('projetos.show', $projeto->id) }}" class="projeto">
            <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" class="projeto-capa" alt="">
            <div class="overlay">
                <span>{{ $projeto->titulo }}</span>
            </div>
        </a>
        @endforeach
    </div>
    <button class="btn-projetos">Ver mais +</button>
</div>

@endsection