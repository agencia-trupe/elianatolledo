@extends('frontend.common.template')

@section('content')

<!-- TELAS GRANDE / TABLET -->
<div class="trabalhos">
    @php
    $count = 0;
    $right = true;
    @endphp
    @foreach($registros as $registro)
    @if($count == 0)
    @if($right)
    <div class="trabalho trabalho-right">
        <img src="{{ asset('assets/img/trabalhos/'.$registro->imagem) }}" alt="">
        <div class="texto-trabalho">
            <p class="trabalho-titulo">{{ $registro->titulo }}</p>
            {!! $registro->texto !!}
        </div>
        @else
        <div class="trabalho trabalho-left">
            <div class="texto-trabalho">
                <p class="trabalho-titulo">{{ $registro->titulo }}</p>
                {!! $registro->texto !!}
            </div>
            <img src="{{ asset('assets/img/trabalhos/'.$registro->imagem) }}" alt="">
            @endif
            @endif
            @php
            $count++;
            @endphp
            @if($count == 1)
        </div>
        @php
        $count = 0;
        $right = !$right;
        @endphp
        @endif
        @endforeach
    </div>
</div>

<!-- TELA CELULAR -->
<div class="trabalhos-mobile">
    @foreach($registros as $registro)
    <div class="trabalho-mobile">
        <img src="{{ asset('assets/img/trabalhos/'.$registro->imagem) }}" alt="">
        <div class="texto-trabalho">
            <p class="trabalho-titulo">{{ $registro->titulo }}</p>
            {!! $registro->texto !!}
        </div>
    </div>
    @endforeach
</div>

@endsection