@extends('frontend.common.template')

@section('content')

<div class="content perfil">
    <div class="foto-texto">
        <img src="{{ asset('assets/img/institucional/'.$institucional->imagem) }}" alt="">
        <div class="perfil-texto">
            {!! $institucional->texto_pt_um !!}
        </div>
    </div>
    <div class="texto-destaque">
        {!! $institucional->texto_destaque !!}
    </div>
    <div class="texto-dois">
        {!! $institucional->texto_pt_dois !!}
    </div>

    <hr>

    <div class="equipe">
        @foreach($equipe as $pessoa)
        <div class="pessoa">
            <img src="{{ asset('assets/img/equipe/'.$pessoa->imagem) }}" alt="">
            <div class="texto-pessoa">
                <p class="pessoa-nome">{{ $pessoa->nome }}</p>
                {!! $pessoa->texto !!}
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection