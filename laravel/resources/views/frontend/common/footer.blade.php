<footer>
    <hr>
    <div class="footer">

        <a href="{{ route('home') }}" class="logo-rodape">
            <img src="{{ asset('assets/img/layout/marca-elianaTolledo.png') }}" alt="">
        </a>

        <div class="dados-contato">
            <p class="contato-telefone">{{ $contato->telefone }}</p>
            <div class="end-completo">
                <p>{{ $contato->endereco }} ∙ {{ $contato->bairro }}</p>
                <p>{{ $contato->cep }} ∙ {{ $contato->cidade }}</p>
            </div>
            <a href="{{ $contato->instagram }}" target="_blank" class="instagram">
                <img src="{{ asset('assets/img/layout/icone-instagram-cinza.svg') }}" alt="">
            </a>
        </div>

        <div class="dados-menu">
            <a href="{{ route('quem-somos') }}">Quem Somos</a>
            <a href="{{ route('como-trabalhamos') }}">Como Trabalhamos</a>
            <a href="{{ route('projetos.index') }}">Projetos Desenvolvidos</a>
            <a href="{{ route('referencias-clientes') }}">Referências e Clientes</a>
            <a href="{{ route('contato') }}">Contato</a>
        </div>

        <div class="info-trupe">
            <p>© {{ date('Y') }} {{ config('app.name') }} &middot;</p>
            <p>Todos os direitos reservados</p>
            <p>Criação de sites: <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a></p>
        </div>

    </div>
</footer>