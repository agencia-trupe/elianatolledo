<div class="itens-menu">
    <a href="{{ route('quem-somos') }}" @if(Tools::routeIs('quem-somos')) class="active" @endif>Quem Somos</a>
    <img src="{{ asset('assets/img/layout/barra-vertical.png') }}" alt="" class="barra-menu">
    <a href="{{ route('como-trabalhamos') }}" @if(Tools::routeIs('como-trabalhamos')) class="active" @endif>Como Trabalhamos</a>
    <img src="{{ asset('assets/img/layout/barra-vertical.png') }}" alt="" class="barra-menu">
    <a href="{{ route('projetos.index') }}" @if(Tools::routeIs('projetos.*')) class="active" @endif>Projetos Desenvolvidos</a>
    <img src="{{ asset('assets/img/layout/barra-vertical.png') }}" alt="" class="barra-menu">
    <a href="{{ route('referencias-clientes') }}" @if(Tools::routeIs('referencias-clientes')) class="active" @endif>Referências e Clientes</a>
    <img src="{{ asset('assets/img/layout/barra-vertical.png') }}" alt="" class="barra-menu">
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
    <img src="{{ asset('assets/img/layout/barra-vertical.png') }}" alt="" class="barra-menu">
    @if($contato->instagram)
    <div class="social">
        @if($contato->instagram)
        <a href="{{ $contato->instagram }}" target="_blank" class="instagram"></a>
        @endif
    </div>
    @endif
</div>