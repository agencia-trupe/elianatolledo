import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
  // BANNERS + PAGERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    pager: ".cycle-pager",
  });

  var quantBanners = $(".banner").length;
  var widthPagers = 100 / quantBanners + "%";
  $(".cycle-pager > span").css("width", widthPagers);

  // GRID REFERENCIAS E CLIENTES
  var $grid = $(".masonry-grid")
    .masonry({
      itemSelector: ".grid-item",
      columnWidth: ".grid-item",
      percentPosition: true,
    })
    .on("layoutComplete", function (event, laidOutItems) {
      $(".masonry-grid").masonry("layout");
    });
  $grid.imagesLoaded().progress(function () {
    $grid.masonry("layout");
  });

  // BTN VER MAIS - PROJETOS
  var itensProjetos = $(".projeto");
  var spliceItensProjetos = 6;
  if (itensProjetos.length <= spliceItensProjetos) {
    $(".btn-projetos").hide();
  }
  var setDivProjetos = function () {
    var spliceItens = itensProjetos.splice(0, spliceItensProjetos);
    $(".center").append(spliceItens);
    $(spliceItens).show();
    if (itensProjetos.length <= 0) {
      $(".btn-projetos").hide();
    }
  };
  $(".btn-projetos").click(function () {
    setDivProjetos();
  });
  $(".projeto").hide();
  setDivProjetos();

  // BTN VER MAIS - REFERENCIAS E CLIENTES
  var itensReferencias = $(".referencia");
  var spliceItensReferencias = 6;
  if (itensReferencias.length <= spliceItensReferencias) {
    $(".btn-referencias").hide();
  }
  var setDivReferencias = function () {
    var spliceItens = itensReferencias.splice(0, spliceItensReferencias);
    $(".masonry-grid").append(spliceItens);
    $(spliceItens).show();
    $(".masonry-grid").masonry("layout");
    if (itensReferencias.length <= 0) {
      $(".btn-referencias").hide();
    }
  };
  $(".btn-referencias").click(function () {
    setDivReferencias();
  });
  $(".referencia").hide();
  setDivReferencias();
});
